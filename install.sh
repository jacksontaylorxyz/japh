#!/bin/sh

# Get the current directory that JAPH is stored under
curr_dir=$(pwd)
# Append ~/.bashrc with 
echo "source $curr_dir/japh" >> ~/.bashrc