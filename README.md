# JAPH - Jackson's Awesome Project Helper

## Purpose
JAPH is a shell command line utility to help with maneuvering your files and building project specific commands in the form of bash functions.

## Installation
### Manual installation
There is an install script (`install.sh`) that you can use. These are the steps that it uses, however.

You will need to `source` the japh file from inside your `$HOME/.bashrc` wherever you have it installed.

If you have the japh repo stored in `$HOME/code/japh`, for example, then in your bashrc file you would put:
```
source $HOME/code/japh/japh
```
Yes, that is `japh/japh`. The first `japh` is the directory, the second is the file that the function actually lives in.

You also need to make sure that you have write access to the directory that japh is stored. For now, it simply puts all the commands it makes into a file called `cmds` in the same directory.

### <span>install</span>.sh
This is a basic script which adds the source command to your bashrc.

To run:
```
$ ./install
```

## What is a "command"?
Commands are the functions that japh runs, these are created by the user. I.E. changing directories, running build commands, etc.

## Usage
- Add a new project
```
[bashprompt] $ japh add projectname
```
- Switch to a project
```
[bashprompt] $ japh projectname
```

- Create new command
```
[bashprompt] $ japh n commandName
```
This will open a buffer in vim to write bash for a more complex function.

- Run Command
```
[bashprompt] $ japh r commandName
```

## Troubleshooting

### Installation
> When I run japh, it says it isn't found.

Make sure nothing in your environment is "unsetting" the japh function. Start by checking your bashrc file and any files you source from there.

> I'm getting an error when opening a terminal

This could be several things:
1. It could be that the version of japh you have cloned has an issue.
   cd to the directory where japh is stored and try sourcing the file yourself to see if there is any errors from there.
2. You could have the wrong path when sourcing.
   Check the path by running the source command which is in your bashrc from your home directory.
3. You may be running the incorrect shell or version of bash
   Ensure that you are running bash and not something like dash, mksh, zsh or fish. I am using bash version 4.4.19 on windows at the time of writing this.

### Usage
> japh doesn't take me to the right directory.

Navigate to where japh is installed, and check the cmds file. From there you can check to see where the project is set to take you. BE CAREFUL EDITING THIS FILE, you can prevent japh from being able to work at all.

> I don't know how to use vim to edit commands

Vim is a command line, modal, text editor. It is a very nice tool to know, and many people use it as their only editor.

Vim is the editor I chose to use to edit commands. This is because it is blocking. Vim will stop japh from running until your file is done being written, and then create the command once you have quite out of vim. I tried with emacs, and vscode and neither worked in the way that I needed it to for japh.

For basic usage you will hit `i` to enter `insert mode`, write your command, hit `escape` and then type `:wq`.
